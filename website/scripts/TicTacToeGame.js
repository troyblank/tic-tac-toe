//-------------------------------------------------------------------------------------------------
//TicTacToe Game v 0.0.1 | a simple game developed for Cox Media Group
//
//This is a one player game of Tic Tac Toe, the user always starts the game, and the computer will
//play by a specific strategy of always occupying the middle if it can, watching for any setups
//for two ways to win, and blocking any third spot in a user taken two in a row.
//-------------------------------------------------------------------------------------------------
var TTTGame = {

    usersTurn: true,
    turnsTaken: 0,
    gamesPlayed: 0,
    gamesTied: 0,
    gamesLost: 0,

    board: [], //represents the status of the nine boxes (0 is player, 1 is computer)

    NUMBER_OF_BOXES: 9,
    TURNS_NEEDED_BEFORE_POSSIBLE_WIN: 5,
    COMPUTER_LAG: 750,

    WINNING_COMBOS: [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ],

    CORNER_BOXES: new Array(0, 2, 6, 8),
    BORDER_BOXES_W_CORNER_RELATIONSHIPS: [{'box':1, 'corners':[0, 2]}, {'box':3, 'corners':[0, 6]}, {'box':5, 'corners':[2, 8]}, {'box':7, 'corners':[6, 8]}],

    //if center is taken by the computer there can only be one strategy to win if the computer always blocks the third spot in a two in a row.
    //placement is a check if the user matches; then place computer on box specified by the reaction value.
    DEATH_GRIP_COMBOS_FOR_CENTER_TAKEN: [{
        'placement': [0, 8],
        'reaction': 1
    }, {
        'placement': [2, 6],
        'reaction': 1
    }],

    init: function() {
        TTTGame.addListeners();
    },

    addListeners: function() {
        $('.box').on('click', TTTGame.boxPressHand);
        $('button.reset').on('click', TTTGame.resetGame);
        $('button.replay').on('click', TTTGame.clearOutGame);
    },

    //VIEW
    //---------------------------------------------------------------------------------------------
    addDisplayToken: function(targ, letter) {
        //display the X or O
        $(targ).addClass(letter);
        $(targ).addClass('in-play');
    },

    highlightWinRow: function(winningCombo) {
        var i = winningCombo.length - 1;
        while (i >= 0) {
            $($('.box')[winningCombo[i]]).addClass('winner');
            i--;
        }
    },

    showPrompot: function(message) {
        $('.prompt').css('display', 'block');
        $('.prompt h1').html(message);
        $('.prompt').stop().animate({
            'top': 0
        }, 500);
    },

    hidePrompt: function(message) {
        $('.prompt').stop().animate({
            'top': -150
        }, 500, function() {
            $(this).css('display', 'none');
        });
    },

    updateScoreBoard: function() {
        $('.total-count').html(TTTGame.gamesPlayed);
        $('.tied-count').html(TTTGame.gamesTied);
        $('.lost-count').html(TTTGame.gamesLost);
    },

    //CONTROL
    //---------------------------------------------------------------------------------------------
    declareComputerWin: function(winningCombo) {
        TTTGame.gamesPlayed++;
        TTTGame.gamesLost++;

        TTTGame.highlightWinRow(winningCombo);
        TTTGame.showPrompot('You Lost!');
        TTTGame.updateScoreBoard();
    },

    declareTie: function() {
        TTTGame.gamesPlayed++;
        TTTGame.gamesTied++;

        TTTGame.showPrompot('You Tied!');
        TTTGame.updateScoreBoard();
    },

    recordTurn: function(i, token) {
        TTTGame.turnsTaken++;

        TTTGame.board[i] = token;
        var finished = TTTGame.checkForFinishOfGame();
        if (!finished) {
            TTTGame.usersTurn = true;
        }
    },

    checkForFinishOfGame: function() {
        //returns true if game has conclusion
        var finished;
        if (TTTGame.turnsTaken >= TTTGame.TURNS_NEEDED_BEFORE_POSSIBLE_WIN) {
            finished = TTTGame.HasComputerWon();
            if (!finished) {
                finished = TTTGame.IsGameTied();
            }
        }

        return finished;
    },

    HasComputerWon: function() {
        var i = TTTGame.WINNING_COMBOS.length - 1;
        while (i >= 0) {
            var winningCombo = TTTGame.WINNING_COMBOS[i];
            var j = winningCombo.length - 1;
            while (j >= 0) {
                var box = winningCombo[j];
                if (TTTGame.board[box] != 1) {
                    //not a winning combo
                    break;
                } else if (j == 0) {
                    //is a winning combo, bail out of loops
                    TTTGame.declareComputerWin(winningCombo);
                    return true;
                }
                j--;
            }
            i--;
        }
        return false;
    },

    IsGameTied: function() {
        //this assumes a check for computer win occured before.
        if (TTTGame.turnsTaken == TTTGame.NUMBER_OF_BOXES) {
            TTTGame.declareTie();
            return true;
        }
        return false;
    },

    areCornersTaken: function() {
        var i = TTTGame.CORNER_BOXES.length - 1;
        while (i >= 0) {
            if (TTTGame.board[TTTGame.CORNER_BOXES[i]] === undefined) {
                return false;
            }
            i--;
        }

        return true;
    },

    //AI
    computersTurn: function() {
        TTTGame.usersTurn = false;

        //giving computer time to 'think' for user experience/expectation sake.
        setTimeout(function() {
            //we could use a Minimax theorem here, but because traditional tic tac toe is a simple 3x3 grid
            //we will use a bit of 'static' logic with a strategy of trying to take the middle on the first turn to speed up the app.

            //always take the middle if not taken
            if (TTTGame.turnsTaken <= 1) {
                TTTGame.computersFirstTurn();
            } else {
                //computer checks based on who has center box
                if (TTTGame.board[4] == 1) {
                    TTTGame.computersNormalTurn(TTTGame.DEATH_GRIP_COMBOS_FOR_CENTER_TAKEN);
                } else {
                    TTTGame.computersNormalTurn();
                }
            }
        }, TTTGame.COMPUTER_LAG);
    },

    computersFirstTurn: function() {
        if (TTTGame.board[4] === undefined) {
            TTTGame.placeComputerToken(4);
        } else {
            TTTGame.placeComputerTokenInARandomCorner();
        }
    },

    computersNormalTurn: function(deathGripCombos) {
        //order of operations: 1. try to win, 2. try to stop death grip patterns 3. try to block a user win, 4. random placement, with corners being first picks.

        var winReaction = TTTGame.checkForWinPlacement();
        if (winReaction) {
            TTTGame.placeComputerToken(winReaction.box);
        } else {
            var deathGripReaction = deathGripCombos != undefined ? TTTGame.checkForDeathGrip(deathGripCombos) : false;
            if (deathGripReaction) {
                TTTGame.placeComputerToken(deathGripReaction.box);
            } else {
                var blockReaction = TTTGame.checkForWinBlock();
                if (blockReaction) {
                    TTTGame.placeComputerToken(blockReaction.box);
                } else {
                    //corners are power take them first
                    var cornersTaken = TTTGame.areCornersTaken();
                    if (cornersTaken) {
                        TTTGame.placeComputerTokenRandomly();
                    } else {
                        TTTGame.placeComputerTokenInARandomCorner();
                    }
                }
            }

        }
    },

    checkForWinPlacement: function() {
        //returns empty box index if a computer win move could be found
        var winBox = TTTGame.checkForPossibleWinRow(1);
        if (winBox) {
            return winBox;
        }

        return false;
    },

    checkForWinBlock: function() {
        //returns empty box index if a block to a user win move could be found
        var winBox = TTTGame.checkForPossibleWinRow(0);
        if (winBox) {
            return winBox;
        }

        return false;
    },

    checkForPossibleWinRow: function(side) {
        //takes a parameter of 0(user) or 1(computer) and returns and empy box if it could be used to win given a turn by the parameter
        if (TTTGame.turnsTaken >= (TTTGame.TURNS_NEEDED_BEFORE_POSSIBLE_WIN - 2)) {
            var i = TTTGame.WINNING_COMBOS.length - 1;
            while (i >= 0) {
                var j = TTTGame.WINNING_COMBOS[i].length - 1;
                var rowCount = 0;
                var emptyBox = null;
                while (j >= 0) {
                    var box = TTTGame.WINNING_COMBOS[i][j];
                    if (TTTGame.board[box] == side) {
                        rowCount++;
                    } else if (TTTGame.board[box] === undefined) {
                        emptyBox = box;
                    } else {
                        rowCount--;
                    }
                    j--;
                }
                if (rowCount == 2) {
                    return {
                        'box': emptyBox
                    };
                }
                i--;
            }
        }

        return false;
    },

    checkForDeathGrip: function(deathGrip) {
        //returns empty box index if a block is found to a user setting up two ways to win
        var i = deathGrip.length - 1;
        while (i >= 0) {
            //guilting until proven innocent
            var isDeathGrip = true;
            var node = deathGrip[i];

            var j = node['placement'].length - 1;
            while (j >= 0) {
                if (TTTGame.board[node['placement'][j]] != 0) {
                    isDeathGrip = false;
                    break;
                }

                if (j == 0 && isDeathGrip && TTTGame.board[node['reaction']] != 1) {
                    //death grip found, reaction box is empty and ready to take
                    return {
                        'box': node['reaction']
                    };
                }

                j--;
            }

            i--;
        }
        return false;
    },

    placeComputerToken: function(i) {
        TTTGame.addDisplayToken($('.box')[i], 'o');
        TTTGame.recordTurn(i, 1);
    },

    placeComputerTokenInARandomCorner: function() {
        //pick corners around any Xs that exist on the edge as a priority
        var emptyBoxFound = TTTGame.tryAndPlaceComputerTokenInCornerNextToX();

        while (!emptyBoxFound) {
            var randomI = Math.round(BaseUtil.randomRange(0, 3));
            var corner = TTTGame.CORNER_BOXES[randomI]
            if (TTTGame.isBoxEmpty(corner)) {
                TTTGame.placeComputerToken(corner);
                emptyBoxFound = true;
            }
        }
    },

    tryAndPlaceComputerTokenInCornerNextToX: function(){
        var i = TTTGame.BORDER_BOXES_W_CORNER_RELATIONSHIPS.length -1;
        while (i >= 0) {
            var node = TTTGame.BORDER_BOXES_W_CORNER_RELATIONSHIPS[i];
            if(TTTGame.board[node.box] == 0){
                var j = node.corners.length -1;
                while (j >= 0) {
                    var corner = node.corners[j];
                    if (TTTGame.isBoxEmpty(corner)) {
                        TTTGame.placeComputerToken(corner);
                        return true;
                    }
                    j--;
                }
            }
            i--;
        }
        return false;
    },

    placeComputerTokenRandomly: function() {
        var emptyBoxFound = false;

        while (!emptyBoxFound) {
            var randomI = Math.round(BaseUtil.randomRange(0, 9));
            if (TTTGame.isBoxEmpty(randomI)) {
                TTTGame.placeComputerToken(randomI);
                emptyBoxFound = true;
            }
        }
    },

    isBoxEmpty: function(i) {
        return TTTGame.board[i] === undefined;
    },

    //HANDLERS
    //---------------------------------------------------------------------------------------------
    boxPressHand: function() {
        var i = $(this).index();
        if (TTTGame.usersTurn && TTTGame.board[i] === undefined) {
            TTTGame.addDisplayToken(this, 'x');
            TTTGame.recordTurn(i, 0);
            if (TTTGame.turnsTaken < TTTGame.NUMBER_OF_BOXES) {
                TTTGame.computersTurn();
            }
        }
    },

    //DESTROY
    //---------------------------------------------------------------------------------------------
    resetGame: function() {
        TTTGame.gamesPlayed = 0;
        TTTGame.gamesTied = 0;
        TTTGame.gamesLost = 0;

        TTTGame.clearOutGame();
        TTTGame.updateScoreBoard();
    },

    clearOutGame: function() {
        TTTGame.usersTurn = true;
        TTTGame.turnsTaken = 0;

        TTTGame.board = new Array();
        TTTGame.clearTokens();
        TTTGame.hidePrompt();
    },

    clearTokens: function() {
        $('.box').removeClass('x o in-play winner');
    }

}

$(document).ready(TTTGame.init);